#!/bin/sh

set -e

if [ $# -ne 2 -o "$1" = "-h" -o "$1" = "--help" ]; then
	cat << EOF
Script for creating python project.

This script creates basic project structure in ${HOME}/Develop/python dir.
Project consists form following directories and files:
	./bin/
	./config/
	./docs/
	./src/
	./tests/
	./venv/
	./setup.py
	./setup.cfg
	./requirements.txt
	./.git
	./.gitignore

USAGE:
	$ devel_env.sh <PYTHON> <PROJECT_NAME>

POSITIONAL ARGUMENTS:

	PYTHON: Python interpreter for which venv will be created.
	PROJECT_NAME: Name of project. This name is used on several placess,
		      like ${PROJECT_DIR}, name in setup.py, etc.
EOF
	exit 1
fi


PYTHON="$1"

command -v git >/dev/null 2>&1 || { echo >&2 "Script requires installed git version controll. Aborting."; exit 1; }
command -v $PYTHON >/dev/null 2>&1 || { echo >&2 "Script requires installed version of python interpreter. Aborting."; exit 1; }
$PYTHON -m virtualenv --version >/dev/null 2>&1 || { echo >&2 "Script requires python-virtualenv to be installed. Aborting."; exit 1; }


PROJECT_NAME="$2"
PROJECT_DIR_NAME=$(echo ${PROJECT_NAME} | sed -e 's/-\+/_/g')
PROJECTS_ROOT=${3:-${HOME}/Develop/python/}

PROJECT_PATH="${PROJECTS_ROOT}${PROJECT_DIR_NAME}"

echo "Creating \"${PROJECT_PATH}\" ..."

if [ -e ${PROJECT_PATH} ]; then
	echo "Can not create project \"$PROJECT_NAME\" @ \"$PROJECT_PATH\""
	exit 1
fi


mkdir -p ${PROJECT_PATH}
cd ${PROJECT_PATH}


PROJECT_INTERPRETER="$PROJECT_PATH/venv/bin/python"
PIP="$PROJECT_PATH/venv/bin/pip"

if [ ! -e "venv" ]; then
	echo "ENVIRONMENT \"$PROJECT_NAME\" does not exists ... Creating ..."

	echo "creating virtual environment in \"$VENV_PATH\""
	$PYTHON -m virtualenv -p ${PYTHON} venv

	echo "Installing required dependencies"
	$PIP install --upgrade pip setuptools

	$PIP install pylint flake8 pylama autopep8 safety bandit dodgy \
	       	pydocstyle isort pep8-naming scspell3k

	echo "ENVIRONMENT \"$PROJECT_NAME\" created ..."
fi


echo "Creating basic project structure ..."

mkdir -p "${PROJECT_PATH}/src/${PROJECT_DIR_NAME}"
mkdir -p "${PROJECT_PATH}/bin"
mkdir -p "${PROJECT_PATH}/config"
mkdir -p "${PROJECT_PATH}/tests"
mkdir -p "${PROJECT_PATH}/docs"

touch ${PROJECT_PATH}/config/.gitkeep
touch "${PROJECT_PATH}/tests/.gitkeep"
touch ${PROJECT_PATH}/docs/.gitkeep
touch ${PROJECT_PATH}/requirements.txt
# Create main.py
cat > ${PROJECT_PATH}/bin/main.py <<-EOF
#!/usr/bin/env ${PYTHON}


if __name__ == '__main__':
    print("Hello, World!")
EOF

chmod +x ${PROJECT_PATH}/bin/main.py

# Create __init__.py in module
cat > "${PROJECT_PATH}/src/${PROJECT_DIR_NAME}/__init__.py" <<-EOF
#!/usr/bin/env ${PYTHON}

__all__ = []
EOF

cat > ${PROJECT_PATH}/setup.cfg <<-EOF
[aliases]
test = pytest

[tool:pytest]
addopts = --verbose
python_files = tests/test_*.py
EOF

echo "#!/usr/bin/env python" > ${PROJECT_PATH}/src/${PROJECT_DIR_NAME}/__init__.py

cat >${PROJECT_PATH}/setup.py <<-EOF
#!/usr/bin/env ${PYTHON}
from setuptools import (
    setup,
    find_packages
)


setup(
    name='${PROJECT_NAME}',
    version_format='{tag}.dev{commitcount}+{gitsha}',
    author='Author Name Placeholder',
    author_email='author.email@example.com',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    setup_requires=['pytest-runner', 'setuptools-git-version'],
    tests_require=['pytest'],
    extras_require={
        'docs': [
            'sphinx',
            'sphinxcontrib-napoleon'
        ],
        'test': [
            'pytest',
            'pytest-runner'
        ]
    }
)
EOF

chmod +x ${PROJECT_PATH}/setup.py

cat >${PROJECT_PATH}/README.rst <<-EOF
${PROJECT_NAME}
$(echo ${PROJECT_NAME} | sed -e 's/\S/#/g')

!blank
EOF

cat >${PROJECT_PATH}/.gitignore <<-EOF
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class

# C extensions
*.so

# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
share/python-wheels/
*.egg-info/
.installed.cfg
*.egg
MANIFEST

# PyInstaller
#  Usually these files are written by a python script from a template
#  before PyInstaller builds the exe, so as to inject date/other infos into it.
*.manifest
*.spec

# Installer logs
pip-log.txt
pip-delete-this-directory.txt

# Unit test / coverage reports
htmlcov/
.tox/
.nox/
.coverage
.coverage.*
.cache
nosetests.xml
coverage.xml
*.cover
.hypothesis/
.pytest_cache/

# Translations
*.mo
*.pot

# Django stuff:
*.log
local_settings.py
db.sqlite3

# Flask stuff:
instance/
.webassets-cache

# Scrapy stuff:
.scrapy

# Sphinx documentation
docs/_build/

# PyBuilder
target/

# Jupyter Notebook
.ipynb_checkpoints

# IPython
profile_default/
ipython_config.py

# pyenv
.python-version

# celery beat schedule file
celerybeat-schedule

# SageMath parsed files
*.sage.py

# Environments
.env
.venv
env/
venv/
ENV/
env.bak/
venv.bak/

# Spyder project settings
.spyderproject
.spyproject

# Rope project settings
.ropeproject

# mkdocs documentation
/site

# mypy
.mypy_cache/
.dmypy.json
dmypy.json

# Pyre type checker
.pyre/

# Remove all idea files
.idea/*

tags
gems.tags

# Ignore vim files
# Swap
[._]*.s[a-v][a-z]
[._]*.sw[a-p]
[._]s[a-rt-v][a-z]
[._]ss[a-gi-z]
[._]sw[a-p]

# Session
Session.vim

# Temporary
.netrwhist
*~
# Auto-generated tag files
tags
# Persistent undo
[._]*.un~
EOF

echo "Creating git version control ..."
git init

echo "Adding all files that are not in .gitignore"
git add .

echo "Commiting ..."
git commit -m "Initial commit for project ${PROJECT_NAME}"

echo "Tagging current initial version"
git tag -a v0.1.0 -m "Initial version ${PROJECT_NAME}-0.1.0"
