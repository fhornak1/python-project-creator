python-project-creator
======================

Shell utility for creating python project structure, creating virtualenv and enabling version control.


Usage
-----

*ppc_create* arguments and options:

  .. code-block:: sh

        $ ./ppc_create.sh <PYTHON_INTERPRETER> <PROJECT_NAME> [PYTHON_PROJECTS_BASE_PATH]


Default path for *PYTHON_PROJECTS_BASE_PATH* is *$HOME/Develop/python*.


Example
-------

Create *foo-python-project* to default projects base dir:

  .. code-block:: sh

        $ ./ppc_create.sh python3 foo-python-project
