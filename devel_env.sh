#!/bin/sh -e

if [ $# -ne 1 -o "$1" = "-h" -o "$1" = "--help" ]; then
	cat << EOF
Script for creating development virtualenv.

This script creates 'requirements-dev.txt' most often used tools and liters.

USAGE:
	$ devel_env.sh <PYTHON>

POSITIONAL ARGUMENTS:

	PYTHON: Python interpreter for which venv will be created.
EOF
	exit 1
fi

PYTHON="$1"

command -v "$PYTHON" >/dev/null 2>&1 || { echo >&2 "Script requires installed version of ${PYTHON} interpreter. Aborting."; exit 1; }

"$PYTHON" -m virtualenv --version >/dev/null 2>&1 || { echo >&2 "Script requires installed virtualenv. Aborting."; exit 1; }

if [ ! -f ./requirements-dev.txt ]; then
	cat > ./requirements-dev.txt <<-EOF
pylint
flake8
flake8-import-order
flake8-blind-except
flake8-builtins
flake8-docstrings
vulture
autopep8
safety
bandit
dodgy
isort
pep8-naming
scspell3k
EOF
fi


if [ ! -d venv ]; then
	echo "Creating virtualenv in $(pwd)/venv ..."
	"$PYTHON" -m virtualenv -p "${PYTHON}"
fi

echo 'Installing/Upgrading basic dependencies ...'

./venv/bin/pip install --upgrade -r requirements-dev.txt
